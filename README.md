# World Converter 1.17

Plugin to automatically regenerate missing parts of an old Minecraft world. In
1.17 (with datapack) the world height was expanded, so the old chunks have to
regenerated above level 5.
