package world.urelion.worldconverter117;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.Singleton;
import lombok.Singleton.Style;
import lombok.extern.slf4j.Slf4j;
import net.milkbowl.vault.permission.Permission;
import org.bstats.bukkit.Metrics;
import org.bstats.charts.SingleLineChart;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.World.Environment;
import org.bukkit.command.PluginCommand;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.graalvm.compiler.core.common.SuppressFBWarnings;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.Timer;

@Singleton(style = Style.HOLDER)
@Slf4j
public final class WorldConverter117Plugin
extends JavaPlugin {
	@SuppressFBWarnings(
		value = {
			"NP_NONNULL_FIELD_NOT_INITIALIZED_IN_CONSTRUCTOR",
			"MS_EXPOSE_REP"
		},
		justification =
			"Attribute will be initialized on plugin load;" +
			"Getter is generated by Lombok."
	)
	@Getter
	@Setter(AccessLevel.PRIVATE)
	@NotNull
	private static WorldConverter117Plugin INSTANCE;

	@Getter
	@Setter(AccessLevel.PRIVATE)
	private Permission permissions;

	public static int logToPlayers(
		@NotNull final String message
	) {
		WorldConverter117Plugin.log.debug(
			"Logging message to players: " +
			message
		);

		int i = 0;

		for (Player player : Bukkit.getOnlinePlayers()) {
			if (WorldConverter117Plugin.getINSTANCE().getPermissions().has(
				player,
				"worldconverter117.message"
			)) {
				player.sendMessage(message);
				i++;
			}
		}

		WorldConverter117Plugin.log.debug(
			"Logged message to " +
			i +
			" players."
		);

		return i;
	}

	@Override
	public void onLoad() {
		WorldConverter117Plugin.log.debug("Loading World Converter 1.17...");

		super.onLoad();

		WorldConverter117Plugin.log.debug("Create plugin instance.");

		WorldConverter117Plugin.setINSTANCE(this);

		File dataFolder = this.getDataFolder();
		if (dataFolder.exists()) {
			WorldConverter117Plugin.log.debug("Data folder already exist.");
		} else {
			WorldConverter117Plugin.log.debug(
				"Create plugins missing data folder."
			);

			if (dataFolder.mkdirs()) {
				WorldConverter117Plugin.log.info(
					"Data folder successfully created."
				);
			} else {
				WorldConverter117Plugin.log.error(
					"Couldn't create plugins data folder."
				);
			}
		}

		WorldConverter117Plugin.log.debug("Setting default configuration...");

		FileConfiguration fileConfig = this.getConfig();
		fileConfig.addDefault("convertAllChunks", false);
		fileConfig.addDefault("convertChunksOnLoad", true);
		fileConfig.options().copyDefaults(true);
		this.saveConfig();

		WorldConverter117Plugin.log.debug("Default configuration set.");
	}

	@Override
	public void onEnable() {
		WorldConverter117Plugin.log.debug("Enabling World Converter 1.17...");

		super.onEnable();

		WorldConverter117Plugin.log.debug("Setup permissions handler.");
		this.setupPermissions();

		WorldConverter117Plugin.log.debug("Registers ChunkListener.");
		Bukkit.getPluginManager().registerEvents(
			ChunkListener.getINSTANCE(),
			this
		);

		WorldConverter117Plugin.log.debug("Registers CommandListener.");

		final PluginCommand wcCommand = this.getCommand("worldconverter");

		if (wcCommand != null) {
			CommandListener commandListener = CommandListener.getINSTANCE();

			wcCommand.setExecutor(commandListener);
			wcCommand.setTabCompleter(commandListener);
		} else {
			WorldConverter117Plugin.log.error(
				"Missing command /worldconverter !"
			);
		}

		WorldConverter117Plugin.log.debug("Initialize WorldConverter.");

		final WorldConverter worldConverter = WorldConverter.getINSTANCE();

		if (this.getConfig().getBoolean("convertAllChunks")) {
			worldConverter.loadChunksFromFile();
		}

		WorldConverter117Plugin.log.debug("Initialize bStats metrics.");

		int     bStatsPluginId = 12703;
		Metrics metrics        = new Metrics(this, bStatsPluginId);
		metrics.addCustomChart(new SingleLineChart(
			"oldChunks",
			worldConverter::countOldChunks
		));
		metrics.addCustomChart(new SingleLineChart(
			"newChunks",
			worldConverter::countNewChunks
		));
		metrics.addCustomChart(new SingleLineChart(
			"wrongChunks",
			worldConverter::countWrongChunks
		));
		metrics.addCustomChart(new SingleLineChart(
			"allChunks",
			() -> worldConverter.countOldChunks() +
				  worldConverter.countNewChunks() +
				  worldConverter.countWrongChunks()
		));
		metrics.addCustomChart(new SingleLineChart(
			"timers",
			worldConverter::countTimers
		));
		metrics.addCustomChart(new SingleLineChart(
			"worlds",
			() -> {
				int count = 0;

				for (World world : Bukkit.getWorlds()) {
					if (world.getEnvironment() == Environment.NORMAL) {
						count++;
					}
				}

				return count;
			}
		));

		WorldConverter117Plugin.log.debug("Load all worlds from files.");

		for (World world : Bukkit.getWorlds()) {
			String      worldName   = world.getName();
			Environment environment = world.getEnvironment();

			WorldConverter117Plugin.log.debug(
				"World " + worldName +
				" is environment " + environment +
				"."
			);

			if (environment != Environment.NORMAL) {
				WorldConverter117Plugin.log.info(
					"Environment of world " + worldName +
					" is not " + Environment.NORMAL +
					". Skipping."
				);

				continue;
			}

			WorldConverter117Plugin.log.info(
				"Load chunks of world " +
				worldName +
				"."
			);

			//worldConverter.listAllChunks(world);

			worldConverter.addOldChunk(ConverterChunk.getChunk(
				world,
				0,
				0
			));
		}

		WorldConverter117Plugin.log.debug("Start conversion.");

		worldConverter.convertNext();

		WorldConverter117Plugin.log.debug("Enabled World Converter 1.17.");
	}

	@Override
	public void onDisable() {
		WorldConverter117Plugin.log.debug("Disabling World Converter 1.17...");

		super.onDisable();

		WorldConverter117Plugin.log.debug("Cancel all active timers.");

		WorldConverter converter = WorldConverter.getINSTANCE();

		for (Timer timer : converter.getTimers()) {
			timer.cancel();
			timer.purge();

			converter.removeTimer(timer);
		}

		WorldConverter117Plugin.log.debug("Save all new chunks to file.");

		converter.saveChunksToFile();

		WorldConverter117Plugin.log.debug("Saving configuration file...");

		this.saveConfig();

		WorldConverter117Plugin.log.debug("Configuration file saved.");

		WorldConverter117Plugin.log.debug("Disabled World Converter 1.17.");
	}

	private boolean setupPermissions() {
		WorldConverter117Plugin.log.debug("Setup permissions system...");

		RegisteredServiceProvider<Permission> registeredServiceProvider =
			this.getServer().getServicesManager().getRegistration(
				Permission.class
			);

		if (registeredServiceProvider == null) {
			WorldConverter117Plugin.log.warn("No permissions handler found!");

			return false;
		}

		this.setPermissions(registeredServiceProvider.getProvider());

		boolean success = this.getPermissions() != null;

		WorldConverter117Plugin.log.debug(
			"Permission system " +
			(success ? "successfully" : "not") +
			" enabled."
		);

		return success;
	}
}
