package world.urelion.worldconverter117;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Singleton;
import lombok.Singleton.Style;
import lombok.extern.slf4j.Slf4j;
import org.bukkit.World;
import org.bukkit.World.Environment;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkLoadEvent;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Slf4j
@Singleton(style = Style.HOLDER)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ChunkListener
implements Listener {
	@Getter
	@NotNull
	private static final ChunkListener INSTANCE = new ChunkListener();

	@EventHandler
	public void onChunkLoad(
		@Nullable final ChunkLoadEvent event
	) {
		ChunkListener.log.debug("Handling chunk load event...");

		if (!(WorldConverter117Plugin.getINSTANCE().getConfig().getBoolean(
			"convertChunksOnLoad"
		))) {
			ChunkListener.log.debug("Converting on chunk loading is disabled!");

			return;
		}

		if (event == null) {
			ChunkListener.log.error("No event given! Abort event handling.");

			return;
		}

		ConverterChunk converterChunk = ConverterChunk.getChunk(
			event.getChunk()
		);
		World world = converterChunk.getWorld();

		ChunkListener.log.debug("Chunk loaded at " + converterChunk + ".");

		Environment environment = world.getEnvironment();
		if (environment != Environment.NORMAL) {
			ChunkListener.log.debug(
				"World " + world.getName() +
				" is in " + environment +
				". Skip conversion."
			);

			return;
		}

		WorldConverter worldConverter = WorldConverter.getINSTANCE();
		if (
			worldConverter.containsOldChunk(converterChunk) ||
			worldConverter.containsNewChunk(converterChunk) ||
			worldConverter.containsWrongChunk(converterChunk)
		) {
			ChunkListener.log.debug("Chunk already covered by converter.");

			return;
		}

		ChunkListener.log.debug("New chunk for converter.");

		if (event.isNewChunk()) {
			ChunkListener.log.debug(
				"Chunk is new generated and don't need to be converted."
			);

			worldConverter.addNewChunk(converterChunk);
		} else {
			ChunkListener.log.debug("Chunk needs to be converted.");

			worldConverter.addOldChunk(converterChunk);
		}
	}
}
