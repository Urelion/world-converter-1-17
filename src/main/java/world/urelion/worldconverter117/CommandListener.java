package world.urelion.worldconverter117;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Singleton;
import lombok.Singleton.Style;
import lombok.extern.slf4j.Slf4j;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Singleton(style = Style.HOLDER)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Slf4j
public class CommandListener
implements CommandExecutor, TabCompleter {
	@Getter
	@NotNull
	private static final CommandListener INSTANCE = new CommandListener();

	private static final int NUM_ON_PAGE = 10;

	@Override
	public boolean onCommand(
		@NotNull final CommandSender sender,
		@NotNull final Command command,
		@NotNull final String label,
		@NotNull final String[] args
	) {
		CommandListener.log.debug("Starting command handling...");

		String commandName = command.getName();

		if ("worldconverter".equalsIgnoreCase(commandName)) {
			CommandListener.log.debug("Handle command /worldconverter.");

			if (args.length < 1) {
				CommandListener.log.warn(
					"No sub-command given! Cancel command handling."
				);

				sender.sendMessage(
					"Sub-command missing. Usage: /" +
					commandName +
					" <list|convert> [page]"
				);

				return false;
			}

			Permission permission =
				WorldConverter117Plugin.getINSTANCE().getPermissions();

			String subCommand = args[0];
			if ("list".equalsIgnoreCase(subCommand)) {
				CommandListener.log.debug("Handle list command.");

				if (!(permission.has(
					sender,
					"worldconverter117.list"
				))) {
					CommandListener.log.debug(
						"CommandSender has no permission to perform " +
						"\"/worldconverter list\" command."
					);

					sender.sendMessage(
						"You don't have access to this command!"
					);

					return false;
				}
				Set<ConverterChunk> oldChunks =
					WorldConverter.getINSTANCE().getOldChunks();
				int chunkCount = oldChunks.size();

				sender.sendMessage(
					"The following " +
					WorldConverter.pluralNum(
						chunkCount,
						"chunk"
					) +
					" should be updated:"
				);

				int page = 1;
				if (args.length >= 2) {
					try {
						page = Integer.parseInt(args[1]);
					} catch (NumberFormatException numberFormatException) {
						CommandListener.log.warn(
							"Given page is not an integer!",
							numberFormatException
						);

						return false;
					}
				}

				int lastPage = (int)(
					Math.ceil((double)chunkCount / CommandListener.NUM_ON_PAGE)
				);
				if (page > lastPage) {
					page = lastPage;
				}

				CommandListener.log.debug(
					"Show page " +
					page +
					" with " +
					CommandListener.NUM_ON_PAGE +
					" entries."
				);

				int pageIndex = (page - 1) * CommandListener.NUM_ON_PAGE;
				for (
					ConverterChunk chunk :
					(new ArrayList<>(oldChunks)).subList(
						pageIndex,
						Math.min(
							pageIndex +
							CommandListener.NUM_ON_PAGE,
							chunkCount
						)
					)
				) {
					sender.sendMessage(chunk.toString());
				}

				sender.sendMessage("Page " + page + " of " + lastPage);
			} else if ("convert".equalsIgnoreCase(subCommand)) {
				CommandListener.log.debug("Handle convert command.");

				if (!(permission.has(
					sender,
					"worldconverter117.convert"
				))) {
					CommandListener.log.debug(
						"CommandSender has no permission to perform " +
						"\"/worldconverter convert\" command."
					);

					sender.sendMessage(
						"You don't have access to this command."
					);

					return false;
				}

				World world;
				int   x, z;

				if (args.length >= 4) {
					try {
						String worldName = args[1];
						world = Bukkit.getWorld(worldName);

						if (world == null) {
							String logMsg =
								"Given world " +
								worldName +
								" is not available!";

							CommandListener.log.warn(logMsg);
							sender.sendMessage(logMsg);

							return false;
						}

						x = Integer.parseInt(args[2]);
						z = Integer.parseInt(args[3]);
					} catch (NumberFormatException numberFormatException) {
						String logMsg =
							"Couldn't parse x or z from command parameters!";

						CommandListener.log.warn(logMsg);
						sender.sendMessage(logMsg);

						return false;
					}
				} else if (sender instanceof Player) {
					Player player = (Player)sender;

					Location location = player.getLocation();
					world			  = location.getWorld();

					if (world == null) {
						CommandListener.log.error(
							"Couldn't find world of player " +
							player.getName()
						);

						return false;
					}

					Chunk chunk = location.getChunk();
					x = chunk.getX();
					z = chunk.getZ();
				} else {
					String logMsg =
						"No given location nor got from player location!";

					CommandListener.log.warn(logMsg);
					sender.sendMessage(logMsg);

					return false;
				}

				ConverterChunk converterChunk = ConverterChunk.getChunk(
					world,
					x,
					z
				);

				CommandListener.log.debug(
					"Converting chunk " +
					converterChunk +
					" by command..."
				);
				sender.sendMessage(
					"Converting your requested chunk " +
					converterChunk +
					"..."
				);

				boolean success = converterChunk.convert();

				String logMsg =
					"Chunk " +
					converterChunk +
					" requested by command was " +
					(success ? "" : "not ") +
					"successful converted.";
				CommandListener.log.debug(logMsg);
				sender.sendMessage(logMsg);

				return true;
			} else {
				String logMsg = "Unknown sub-command given: " + subCommand;

				CommandListener.log.warn(logMsg);
				sender.sendMessage(logMsg);

				return false;
			}
		}

		return true;
	}

	@Override
	@Nullable
	public List<String> onTabComplete(
		@NotNull CommandSender sender,
		@NotNull Command command,
		@NotNull String label,
		@NotNull String[] args
	) {
		if (!("worldconverter".equalsIgnoreCase(command.getName()))) {
			CommandListener.log.debug("Wrong command for this TabCompleter.");

			return null;
		}

		if (!(sender.hasPermission("worldconverter117.command"))) {
			CommandListener.log.warn(
				"Sender has no permission to execute this command!"
			);

			return null;
		}

		Set<String> suggestions = new HashSet<>();

		if (args.length >= 3) {
			if ("convert".equalsIgnoreCase(args[0])) {
				if (!(sender.hasPermission("worldconverter117.convert"))) {
					CommandListener.log.warn(
						"Sender has no permission to execute this sub-command!"
					);

					return null;
				}

				if (sender instanceof Player) {
					Player player = (Player)sender;

					suggestions.add(String.valueOf(
						player.getLocation().getChunk().getZ()
					));
				}
			}
		} else if (args.length >= 2) {
			if ("convert".equalsIgnoreCase(args[0])) {
				if (!(sender.hasPermission("worldconverter117.convert"))) {
					CommandListener.log.warn(
						"Sender has no permission to execute this sub-command!"
					);

					return null;
				}

				if (sender instanceof Player) {
					Player player = (Player)sender;

					suggestions.add(String.valueOf(
						player.getLocation().getChunk().getX()
					));
				}
			}
		} else if (args.length >= 1) {
			String subCommand = args[0];

			if ("list".equalsIgnoreCase(subCommand)) {
				if (!(sender.hasPermission("worldconverter117.list"))) {
					CommandListener.log.warn(
						"Sender has no permission to execute this sub-command!"
					);

					return null;
				}

				int maxPage = Math.min(
					WorldConverter.getINSTANCE().countNewChunks(),
					9
				);

				for (int i = 1; i <= maxPage; i++) {
					suggestions.add(String.valueOf(i));
				}
			} else if ("convert".equalsIgnoreCase(subCommand)) {
				if (!(sender.hasPermission("worldconverter117.convert"))) {
					CommandListener.log.warn(
						"Sender has no permission to execute this sub-command!"
					);

					return null;
				}

				for (World world : Bukkit.getWorlds()) {
					suggestions.add(world.getName());
				}
			}
		} else {
			if (sender.hasPermission("worldconverter117.convert")) {
				suggestions.add("convert");
			}
			if (sender.hasPermission("worldconverter117.list")) {
				suggestions.add("list");
			}
		}

		return new ArrayList<>(suggestions);
	}
}
